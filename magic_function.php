<?php
/*
 * Извините, но я не знаю как при помощи одной функции написать лаконичного,
 * легко читаемого, безопасного и масштабируемого кода.
 */
function linkСonversion(string $url): string
{
    $urlInformation = parse_url($url);

    $query = [];

    parse_str($urlInformation['query'], $query);

    foreach ($query as $key => $value) {
        if (3 === (int) $value) {
            unset($query[$key]);
        }
    }

    krsort($query);

    $query['url'] = $urlInformation['path'];

    $queryString = http_build_query($query);

    return $urlInformation['scheme'] . '://' . $urlInformation['host'] . '?' . $queryString;
}